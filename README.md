# Geojson Antimeridian Cutting

[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/avandesa/geojson-antimeridian-cut)][pipeline]
[![Codecov](https://img.shields.io/codecov/c/gl/avandesa/geojson-antimeridian-cut)][codecov]

[GeoJSON][geojson] is a standard for representing geographic data in a JSON file. `Features` and
`FeatureCollections` are composed of different geometry objects, including:

* `LineString`s
* `MultiLineString`s
* `Polygon`s
* `MultiPolygon`s
* `GeometryCollection`s

It is very likely that some geographic data may cross the Antimeridian (180° E or 180° W).
[RFC 7946 Section 3.1.9][split_section] specifies that such objects SHOULD be broken up into two or
more objects, none of which cross the antimeridian, and which together are all equivalent. This
package implements that splitting.

The API currently provides one function: `splitGeoJSON(object)`, which takes any valid GeoJSON
object and, if it crosses the antimeridian, creates a new Object that is equivalent but does not
cross the antimeridian. `LineString`s may become `MultiLineString`s, and `MultiLineString`s will
gain more entries. The same goes for `Polygon`s and `MultiPolygon`s.

All feature properties and object foreign members are preserved.

Example usage:

```javascript
const splitGeoJSON = require('geojson-antimeridian-cut');

const lineString = {
  type: 'Polygon',
  coordinates: [
    [
      [170, 10],
      [170, -10],
      [-170, -10],
      [-170, 10],
      [170, 10],
    ],
  ],
};

console.log(splitGeoJSON(lineString));
/*
{
  "type": "MultiPolygon",
  "coordinates": [
    [
      [180, 10],
      [170, 10],
      [170, -10],
      [180, -10],
      [180, 10]
    ],
    [
      [-180, -10],
      [-170, -10],
      [-170, 10],
      [-180, 10],
      [-180, -10]
    ]
  ]
}
*/

const feature = {
  type: 'Feature',
  properties: {
    name: 'Anchorage to Tokyo',
  },
  geometry: {
    type: 'LineString',
    coordinates: [
      [-149.89, 61.23],
      [-220.29, 35.69],
    ],
  },
};

console.log(splitGeoJSON(feature));
/*
{
  "type": "Feature",
  "properties": {
    "name": "Anchorage to Tokyo"
  },
  "geometry": {
    "type": "MultiLineString",
    "coordinates": [
      [
        [-149.89, 61.23],
        [-180, 50.31]
      ],
      [
        [-180, 50.31],
        [-220.29, 35.69]
      ]
    ]
  }
}
*/
```

[geojson]: https://tools.ietf.org/html/rfc7946
[split_section]: https://tools.ietf.org/html/rfc7946#section-3.1.9
[pipeline]: https://gitlab.com/avandesa/geojson-antimeridian-cut/pipelines
[codecov]: https://codecov.io/gl/avandesa/geojson-antimeridian-cut/
