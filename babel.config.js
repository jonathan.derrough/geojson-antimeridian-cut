const presets = [
    [
        '@babel/env',
        {
            targets: {
                node: '8',
            },
            useBuiltIns: 'usage',
            corejs: '3.0.0',
        },
    ],
];


module.exports = { presets };
