import { splitFeature, splitGeometryCollection, splitFeatureCollection } from '../../src/geometry/features';

describe('Features', () => {
    test('Should not cut Feature', () => {
        const nullFeature = {
            type: 'Feature',
            properties: null,
            geometry: null,
        };

        const featureWithLineString = {
            type: 'Feature',
            properties: null,
            geometry: {
                type: 'LineString',
                coordinates: [
                    [-1, 1],
                    [1, -1],
                ],
            },
        };

        const featureWithMultiLineString = {
            type: 'Feature',
            properties: null,
            geometry: {
                type: 'MultiLineString',
                coordinates: [
                    [
                        [1, 1],
                        [2, 2],
                    ],
                    [
                        [-1, 0],
                        [1, 1],
                        [-1, 2],
                    ],
                ],
            },
        };

        const featureWithPolygon = {
            type: 'Feature',
            properties: null,
            geometry: {
                type: 'Polygon',
                coordinates: [
                    [
                        [100.0, 0.0],
                        [101.0, 0.0],
                        [101.0, 1.0],
                        [100.0, 1.0],
                        [100.0, 0.0],
                    ],
                ],
            },
        };

        const featureWithMultiPolygon = {
            type: 'Feature',
            properties: null,
            geometry: {
                type: 'MultiPolygon',
                coordinates: [
                    [
                        [
                            [10, 10],
                            [20, 10],
                            [20, 20],
                            [10, 10],
                            [10, 10],
                        ],
                    ],
                    [
                        [
                            [-2, -2],
                            [2, -2],
                            [2, 2],
                            [-2, 2],
                            [-2, -2],
                        ],
                        [
                            [-1, -1],
                            [-1, 1],
                            [1, 0],
                            [-1, -1],
                        ],
                    ],
                ],
            },
        };

        const resultNull = splitFeature(nullFeature);
        const resultLineString = splitFeature(featureWithLineString);
        const resultMultiLineString = splitFeature(featureWithMultiLineString);
        const resultPolygon = splitFeature(featureWithPolygon);
        const resultMultiPolygon = splitFeature(featureWithMultiPolygon);

        expect(resultNull).toEqual(nullFeature);
        expect(resultLineString).toEqual(featureWithLineString);
        expect(resultMultiLineString).toEqual(featureWithMultiLineString);
        expect(resultPolygon).toEqual(featureWithPolygon);
        expect(resultMultiPolygon).toEqual(featureWithMultiPolygon);
    });

    test('Should cut Feature with LineString', () => {
        const featureWithLineString = {
            type: 'Feature',
            properties: null,
            geometry: {
                type: 'LineString',
                coordinates: [
                    [170, 0],
                    [-170, 0],
                ],
            },
        };

        expect(splitFeature(featureWithLineString)).toEqual({
            type: 'Feature',
            properties: null,
            geometry: {
                type: 'MultiLineString',
                coordinates: [
                    [
                        [170, 0],
                        [180, 0],
                    ],
                    [
                        [-180, 0],
                        [-170, 0],
                    ],
                ],
            },
        });
    });

    test('Should cut feature with MultiLineString', () => {
        const featureWithMultiLineString = {
            type: 'Feature',
            properties: null,
            geometry: {
                type: 'MultiLineString',
                coordinates: [
                    [
                        [-1, 0],
                        [1, 0],
                    ],
                    [
                        [170, 0],
                        [-170, 0],
                    ],
                ],
            },
        };

        expect(splitFeature(featureWithMultiLineString)).toEqual({
            type: 'Feature',
            properties: null,
            geometry: {
                type: 'MultiLineString',
                coordinates: [
                    [
                        [-1, 0],
                        [1, 0],
                    ],
                    [
                        [170, 0],
                        [180, 0],
                    ],
                    [
                        [-180, 0],
                        [-170, 0],
                    ],
                ],
            },
        });
    });

    test('Should cut feature with Polygon', () => {
        const featureWithPolygon = {
            type: 'Feature',
            properties: null,
            geometry: {
                type: 'Polygon',
                coordinates: [
                    [
                        [170, -10],
                        [-170, -10],
                        [-170, 10],
                        [170, 10],
                        [170, -10],
                    ],
                ],
            },
        };

        expect(splitFeature(featureWithPolygon)).toEqual({
            type: 'Feature',
            properties: null,
            geometry: {
                type: 'MultiPolygon',
                coordinates: [
                    [
                        [
                            [180, 10],
                            [170, 10],
                            [170, -10],
                            [180, -10],
                            [180, 10],
                        ],
                    ],
                    [
                        [
                            [-180, -10],
                            [-170, -10],
                            [-170, 10],
                            [-180, 10],
                            [-180, -10],
                        ],
                    ],
                ],
            },
        });
    });

    test('Should cut feature with MultiPolygon', () => {

    });

    test('Should cut feature with GeometryCollection', () => {
        const feature = {
            type: 'Feature',
            properties: null,
            geometry: {
                type: 'GeometryCollection',
                geometries: [
                    {
                        type: 'LineString',
                        coordinates: [
                            [170, 0],
                            [-170, 0],
                        ],
                    },
                    {
                        type: 'Polygon',
                        coordinates: [
                            [
                                [170, -10],
                                [-170, -10],
                                [-170, 10],
                                [170, 10],
                                [170, -10],
                            ],
                        ],
                    },
                ],
            },
        };

        expect(splitFeature(feature)).toEqual({
            type: 'Feature',
            properties: null,
            geometry: {
                type: 'GeometryCollection',
                geometries: [
                    {
                        type: 'MultiLineString',
                        coordinates: [
                            [
                                [170, 0],
                                [180, 0],
                            ],
                            [
                                [-180, 0],
                                [-170, 0],
                            ],
                        ],
                    },
                    {
                        type: 'MultiPolygon',
                        coordinates: [
                            [
                                [
                                    [180, 10],
                                    [170, 10],
                                    [170, -10],
                                    [180, -10],
                                    [180, 10],
                                ],
                            ],
                            [
                                [
                                    [-180, -10],
                                    [-170, -10],
                                    [-170, 10],
                                    [-180, 10],
                                    [-180, -10],
                                ],
                            ],
                        ],
                    },
                ],
            },
        });
    });
});

describe('FeatureCollections', () => {
    test('Should not cut any in FeatureCollection', () => {
        const collection = {
            type: 'FeatureCollection',
            features: [
                {
                    type: 'Feature',
                    properties: null,
                    geometry: null,

                },
                {
                    type: 'Feature',
                    properties: null,
                    geometry: {
                        type: 'LineString',
                        coordinates: [
                            [-1, 1],
                            [1, -1],
                        ],
                    },

                },
                {
                    type: 'Feature',
                    properties: null,
                    geometry: {
                        type: 'MultiLineString',
                        coordinates: [
                            [
                                [1, 1],
                                [2, 2],
                            ],
                            [
                                [-1, 0],
                                [1, 1],
                                [-1, 2],
                            ],
                        ],
                    },

                },
                {
                    type: 'Feature',
                    properties: null,
                    geometry: {
                        type: 'Polygon',
                        coordinates: [
                            [
                                [100.0, 0.0],
                                [101.0, 0.0],
                                [101.0, 1.0],
                                [100.0, 1.0],
                                [100.0, 0.0],
                            ],
                        ],
                    },
                },
                {
                    type: 'Feature',
                    properties: null,
                    geometry: {
                        type: 'MultiPolygon',
                        coordinates: [
                            [
                                [
                                    [10, 10],
                                    [20, 10],
                                    [20, 20],
                                    [10, 10],
                                    [10, 10],
                                ],
                            ],
                            [
                                [
                                    [-2, -2],
                                    [2, -2],
                                    [2, 2],
                                    [-2, 2],
                                    [-2, -2],
                                ],
                                [
                                    [-1, -1],
                                    [-1, 1],
                                    [1, 0],
                                    [-1, -1],
                                ],
                            ],
                        ],
                    },
                },
            ],
        };

        const emptyCollection = {
            type: 'FeatureCollection',
            features: [],
        };

        const result = splitFeatureCollection(collection);
        const resultEmpty = splitFeatureCollection(emptyCollection);

        expect(result).toEqual(collection);

        expect(resultEmpty).toEqual(emptyCollection);
    });

    test('Should cut some in FeatureCollection', () => {
        const collection = {
            type: 'FeatureCollection',
            features: [
                {
                    type: 'Feature',
                    properties: null,
                    geometry: {
                        type: 'LineString',
                        coordinates: [
                            [170, 0],
                            [-170, 0],
                        ],
                    },
                },
                {
                    type: 'Feature',
                    properties: null,
                    geometry: {
                        type: 'MultiLineString',
                        coordinates: [
                            [
                                [-1, 0],
                                [1, 0],
                            ],
                            [
                                [170, 0],
                                [-170, 0],
                            ],
                        ],
                    },
                },
                {
                    type: 'Feature',
                    properties: null,
                    geometry: {
                        type: 'Polygon',
                        coordinates: [
                            [
                                [170, -10],
                                [-170, -10],
                                [-170, 10],
                                [170, 10],
                                [170, -10],
                            ],
                        ],
                    },
                },
                {
                    type: 'Feature',
                    properties: null,
                    geometry: {
                        type: 'MultiPolygon',
                        coordinates: [
                            [
                                [
                                    [170, -10],
                                    [-170, -10],
                                    [-170, 10],
                                    [170, 10],
                                    [170, -10],
                                ],
                            ],
                            [
                                [
                                    [40, 50],
                                    [50, 50],
                                    [50, 40],
                                    [40, 40],
                                    [40, 50],
                                ],
                            ],
                        ],
                    },
                },
            ],
        };

        const result = splitFeatureCollection(collection);

        expect(result).toEqual({
            type: 'FeatureCollection',
            features: [
                {
                    type: 'Feature',
                    properties: null,
                    geometry: {
                        type: 'MultiLineString',
                        coordinates: [
                            [
                                [170, 0],
                                [180, 0],
                            ],
                            [
                                [-180, 0],
                                [-170, 0],
                            ],
                        ],
                    },
                },
                {
                    type: 'Feature',
                    properties: null,
                    geometry: {
                        type: 'MultiLineString',
                        coordinates: [
                            [
                                [-1, 0],
                                [1, 0],
                            ],
                            [
                                [170, 0],
                                [180, 0],
                            ],
                            [
                                [-180, 0],
                                [-170, 0],
                            ],
                        ],
                    },
                },
                {
                    type: 'Feature',
                    properties: null,
                    geometry: {
                        type: 'MultiPolygon',
                        coordinates: [
                            [
                                [
                                    [180, 10],
                                    [170, 10],
                                    [170, -10],
                                    [180, -10],
                                    [180, 10],
                                ],
                            ],
                            [
                                [
                                    [-180, -10],
                                    [-170, -10],
                                    [-170, 10],
                                    [-180, 10],
                                    [-180, -10],
                                ],
                            ],
                        ],
                    },
                },
                {
                    type: 'Feature',
                    properties: null,
                    geometry: {
                        type: 'MultiPolygon',
                        coordinates: [
                            [
                                [
                                    [180, 10],
                                    [170, 10],
                                    [170, -10],
                                    [180, -10],
                                    [180, 10],
                                ],
                            ],
                            [
                                [
                                    [-180, -10],
                                    [-170, -10],
                                    [-170, 10],
                                    [-180, 10],
                                    [-180, -10],
                                ],
                            ],
                            [
                                [
                                    [40, 50],
                                    [50, 50],
                                    [50, 40],
                                    [40, 40],
                                    [40, 50],
                                ],
                            ],
                        ],
                    },
                },
            ],
        });
    });
});

describe('GeometryCollections', () => {
    test('Should not cut in GeometryCollection', () => {
        const collection = {
            type: 'GeometryCollection',
            geometries: [
                {
                    type: 'LineString',
                    coordinates: [
                        [0, 0],
                        [1, 1],
                    ],
                },
                {
                    type: 'Polygon',
                    coordinates: [
                        [
                            [40, 50],
                            [50, 50],
                            [50, 40],
                            [40, 40],
                            [40, 50],
                        ],
                    ],
                },
                {
                    type: 'MultiLineString',
                    coordinates: [
                        [
                            [1, 1],
                            [2, 2],
                        ],
                        [
                            [-1, 0],
                            [1, 1],
                            [-1, 2],
                        ],
                    ],
                },
                {
                    type: 'MultiPolygon',
                    coordinates: [
                        [
                            [
                                [10, 10],
                                [20, 10],
                                [20, 20],
                                [10, 10],
                                [10, 10],
                            ],
                        ],
                        [
                            [
                                [-2, -2],
                                [2, -2],
                                [2, 2],
                                [-2, 2],
                                [-2, -2],
                            ],
                            [
                                [-1, -1],
                                [-1, 1],
                                [1, 0],
                                [-1, -1],
                            ],
                        ],
                    ],
                },
            ],
        };

        const empty = {
            type: 'GeometryCollection',
            geometries: [],
        };

        const result = splitGeometryCollection(collection);
        const resultEmpty = splitGeometryCollection(empty);

        expect(result).toEqual(collection);
        expect(resultEmpty).toEqual(empty);
    });

    test('Should cut some in GeometryCollection', () => {
        const collection = {
            type: 'GeometryCollection',
            geometries: [
                {
                    type: 'LineString',
                    coordinates: [
                        [170, 0],
                        [-170, 0],
                    ],
                },
                {
                    type: 'Polygon',
                    coordinates: [
                        [
                            [170, -10],
                            [-170, -10],
                            [-170, 10],
                            [170, 10],
                            [170, -10],
                        ],
                    ],
                },
                {
                    type: 'MultiLineString',
                    coordinates: [
                        [
                            [-1, 0],
                            [1, 0],
                        ],
                        [
                            [170, 0],
                            [-170, 0],
                        ],
                    ],
                },
                {
                    type: 'MultiPolygon',
                    coordinates: [
                        [
                            [
                                [170, -10],
                                [-170, -10],
                                [-170, 10],
                                [170, 10],
                                [170, -10],
                            ],
                        ],
                        [
                            [
                                [40, 50],
                                [50, 50],
                                [50, 40],
                                [40, 40],
                                [40, 50],
                            ],
                        ],
                    ],
                },
            ],
        };

        const result = splitGeometryCollection(collection);

        expect(result).toEqual({
            type: 'GeometryCollection',
            geometries: [
                {
                    type: 'MultiLineString',
                    coordinates: [
                        [
                            [170, 0],
                            [180, 0],
                        ],
                        [
                            [-180, 0],
                            [-170, 0],
                        ],
                    ],
                },
                {
                    type: 'MultiPolygon',
                    coordinates: [
                        [
                            [
                                [180, 10],
                                [170, 10],
                                [170, -10],
                                [180, -10],
                                [180, 10],
                            ],
                        ],
                        [
                            [
                                [-180, -10],
                                [-170, -10],
                                [-170, 10],
                                [-180, 10],
                                [-180, -10],
                            ],
                        ],
                    ],
                },
                {
                    type: 'MultiLineString',
                    coordinates: [
                        [
                            [-1, 0],
                            [1, 0],
                        ],
                        [
                            [170, 0],
                            [180, 0],
                        ],
                        [
                            [-180, 0],
                            [-170, 0],
                        ],
                    ],
                },
                {
                    type: 'MultiPolygon',
                    coordinates: [
                        [
                            [
                                [180, 10],
                                [170, 10],
                                [170, -10],
                                [180, -10],
                                [180, 10],
                            ],
                        ],
                        [
                            [
                                [-180, -10],
                                [-170, -10],
                                [-170, 10],
                                [-180, 10],
                                [-180, -10],
                            ],
                        ],
                        [
                            [
                                [40, 50],
                                [50, 50],
                                [50, 40],
                                [40, 40],
                                [40, 50],
                            ],
                        ],
                    ],
                },
            ],
        });
    });
});
