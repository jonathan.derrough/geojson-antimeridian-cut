import { splitLineString, splitMultiLineString, splitCoordinateArray } from '../../src/geometry/lines';

describe('Utils', () => {
    test('Splitting coordinate lists', () => {
        const noCrossing = [
            [1, 0],
            [10, 0],
        ];

        const oneCrossing = [
            [170, 0],
            [-170, 0],
        ];

        const manyCrossings = [
            [-170, 20],
            [170, 0],
            [-170, -20],
        ];

        expect(splitCoordinateArray(noCrossing)).toEqual([noCrossing]);
        expect(splitCoordinateArray(oneCrossing)).toEqual([
            [
                [170, 0],
                [180, 0],
            ],
            [
                [-180, 0],
                [-170, 0],
            ],
        ]);
        expect(splitCoordinateArray(manyCrossings)).toEqual([
            [
                [-170, 20],
                [-180, 10],
            ],
            [
                [180, 10],
                [170, 0],
                [180, -10],
            ],
            [
                [-180, -10],
                [-170, -20],
            ],
        ]);
    });
});

describe('LineStrings', () => {
    test('Should not cut LineStrings', () => {
        const lineStringShort = {
            type: 'LineString',
            coordinates: [
                [0, 0],
                [1, 1],
            ],
        };

        const lineStringLong = {
            type: 'LineString',
            coordinates: [
                [-86.39, 39.80],
                [-84.55, 33.68],
                [-77.08, 38.89],
                [-73.91, 40.74],
                [-87.40, 41.77],
                [-93.25, 44.90],
            ],
        };

        const lineStringCrossesMeridian = {
            type: 'LineString',
            coordinates: [
                [-1, 0],
                [1, 0],
            ],
        };

        const lineStringCrossesMeridianReverse = {
            type: 'LineString',
            coordinates: [
                [1, 0],
                [-1, 0],
            ],
        };

        expect(splitLineString(lineStringShort)).toEqual(lineStringShort);
        expect(splitLineString(lineStringLong)).toEqual(lineStringLong);
        expect(splitLineString(lineStringCrossesMeridian)).toEqual(lineStringCrossesMeridian);
        expect(splitLineString(lineStringCrossesMeridianReverse))
            .toEqual(lineStringCrossesMeridianReverse);
    });

    test('Should cut LineStrings', () => {
        const lineStringShort = {
            type: 'LineString',
            coordinates: [
                [170, 0],
                [-170, 0],
            ],
        };

        const lineStringShortReverse = {
            type: 'LineString',
            coordinates: [
                [-160, 0],
                [160, 0],
            ],
        };

        const lineStringManyCrossings = {
            type: 'LineString',
            coordinates: [
                [-170, 20],
                [170, 0],
                [-170, -20],
            ],
        };

        // A line string that goes to:
        const lineStringLong = {
            type: 'LineString',
            coordinates: [
                [139.79, 35.71], // Tokyo
                [131.83, 43.13], // Vladivostok
                [158.59, 53.04], // Petropavlovsk-Kamchatsky
                [-165.42, 64.51], // Nome
                [-149.99, 61.18], // Anchorage
                [-123.18, 49.32], // Vancouver
            ],
        };

        // The reverse of the line above
        const lineStringLongReverse = {
            type: 'LineString',
            coordinates: [
                [-123.18, 49.32], // Vancouver
                [-149.99, 61.18], // Anchorage
                [-165.42, 64.51], // Nome
                [158.59, 53.04], // Petropavlovsk-Kamchatsky
                [131.83, 43.13], // Vladivostok
                [139.79, 35.71], // Tokyo
            ],
        };

        expect(splitLineString(lineStringShort)).toEqual({
            type: 'MultiLineString',
            coordinates: [
                [
                    [170, 0],
                    [180, 0],
                ],
                [
                    [-180, 0],
                    [-170, 0],
                ],
            ],
        });

        expect(splitLineString(lineStringShortReverse)).toEqual({
            type: 'MultiLineString',
            coordinates: [
                [
                    [-160, 0],
                    [-180, 0],
                ],
                [
                    [180, 0],
                    [160, 0],
                ],
            ],
        });

        expect(splitLineString(lineStringManyCrossings)).toEqual({
            type: 'MultiLineString',
            coordinates: [
                [
                    [-170, 20],
                    [-180, 10],
                ],
                [
                    [180, 10],
                    [170, 0],
                    [180, -10],
                ],
                [
                    [-180, -10],
                    [-170, -20],
                ],
            ],
        });

        expect(splitLineString(lineStringLong)).toEqual({
            type: 'MultiLineString',
            coordinates: [
                [
                    [139.79, 35.71], // Tokyo
                    [131.83, 43.13], // Vladivostok
                    [158.59, 53.04], // Petropavlovsk-Kamchatsky
                    [180, 59.86335926646291],
                ],
                [
                    [-180, 59.86335926646291],
                    [-165.42, 64.51], // Nome
                    [-149.99, 61.18], // Anchorage
                    [-123.18, 49.32], // Vancouver
                ],
            ],
        });

        expect(splitLineString(lineStringLongReverse)).toEqual({
            type: 'MultiLineString',
            coordinates: [
                [
                    [-123.18, 49.32], // Vancouver
                    [-149.99, 61.18], // Anchorage
                    [-165.42, 64.51], // Nome
                    [-180, 59.86335926646291],
                ],
                [
                    [180, 59.86335926646291],
                    [158.59, 53.04], // Petropavlovsk-Kamchatsky
                    [131.83, 43.13], // Vladivostok
                    [139.79, 35.71], // Tokyo
                ],
            ],
        });
    });
});

describe('MultiLineStrings', () => {
    test('Should not cut MultiLineStrings', () => {
        const mls = {
            type: 'MultiLineString',
            coordinates: [
                [
                    [1, 1],
                    [2, 2],
                ],
                [
                    [-1, 0],
                    [1, 1],
                    [-1, 2],
                ],
            ],
        };

        expect(splitMultiLineString(mls)).toEqual(mls);
    });

    test('Should cut MultLineStrings', () => {
        const cutOneOfTwo = {
            type: 'MultiLineString',
            coordinates: [
                [
                    [-1, 0],
                    [1, 0],
                ],
                [
                    [170, 0],
                    [-170, 0],
                ],
            ],
        };

        const cutBoth = {
            type: 'MultiLineString',
            coordinates: [
                [
                    [-160, 30],
                    [160, 20],
                ],
                [
                    [170, 0],
                    [-170, 0],
                ],
            ],
        };

        expect(splitMultiLineString(cutOneOfTwo)).toEqual({
            type: 'MultiLineString',
            coordinates: [
                [
                    [-1, 0],
                    [1, 0],
                ],
                [
                    [170, 0],
                    [180, 0],
                ],
                [
                    [-180, 0],
                    [-170, 0],
                ],
            ],
        });

        expect(splitMultiLineString(cutBoth)).toEqual({
            type: 'MultiLineString',
            coordinates: [
                [
                    [-160, 30],
                    [-180, 25],
                ],
                [
                    [180, 25],
                    [160, 20],
                ],
                [
                    [170, 0],
                    [180, 0],
                ],
                [
                    [-180, 0],
                    [-170, 0],
                ],
            ],
        });
    });
});
